import { User } from "../Schema/User.js";
import bcrypt from "bcrypt";
import { createResult, validateToken } from "../Utils/utility.js";
import {
  LoginValidate,
  UserValidate,
  passwordValidate,
} from "../Middleware/validate.js";
import { createToken } from "../Utils/jwtToken.js";
export const UserRoot = {
 
 
  createUser: async ({ input }) => {
    try {
      const { error } = UserValidate.validate(input);
      if (error) throw new Error(error.details[0].message);

      const isAlreadyRegistered = await User.findOne({
        email: input.email.toLowerCase(),
      });
      if (isAlreadyRegistered) throw new Error("Email id already exists");
      const { email, password, ...rest } = input;
      const hash = await bcrypt.hash(password, 10);
      const data = {
        ...rest,
        email: email.toLowerCase(),
        password: hash,
      };
      const user = new User(data);
      await user.save();
      return createResult({
        data: user,
        message: "User created Successfully",
        status: 201,
      });
    } catch (error) {
      throw error || "Failed to create user";
    }
  },

  
  loginUser: async ({ input }) => {
    const { email, password } = input;
    try {
      const { error } = LoginValidate.validate(input);
      if (error) throw new Error(error.details[0].message);

      const user = await User.findOne({ email });
      if (!user) throw new Error("Invalid Email");
      const hash = await bcrypt.compare(password, user.password);
      if (!hash) throw new Error("Incorrect Password");
      
      const token =await createToken(user);
      return createResult({
        data: user,
        status: 200,
        message: "Logged In successfully",
        token:token
      });
    } catch (err) {
      throw err || "Failed to fetch";
    }
  },
  
  
  getAllUser: async () => {
    try {
      const users = await User.find();

      return users;
    } catch (error) {
      throw new Error("Failed to fetch users");
    }
  },
  

  getUser: async ({ input }) => {
    const { id,token } = input;

    try {
      await validateToken(input.token, id);

      const user = await User.findById(id);
      if (!user) throw new Error("No such user found");
      return createResult({
        data: user,
        message: "User fetched successfully",
        status: 200,
      });
    } catch (err) {
      throw err || "failed to fetch user";
    }
  },
  
  
  updateUser: async ({ input }) => {
    const { id, ...update } = input;
    try {
      await validateToken(input.token,id);
      const user = await User.findByIdAndUpdate(id, update, { new: true });
      if (!user) throw new Error("user not exist");

      return createResult({
        data: user,
        message: "User updated Successfully",
        status: 200,
      });
    } catch (err) {
      throw err || "Update failed";
    }
  },


  deleteUser: async ({ input }) => {
    const { id,token } = input;
    try {
      await validateToken(input.token, id);
      const user = await User.findByIdAndDelete(id);
      if (!user) throw new Error("user not exist");

      return createResult({
        data: user,
        message: user ? "User deleted Successfully" : "user does not exist",
        status: 200,
      });
    } catch (err) {
      throw err || "Update failed";
    }
  },
  
  
  changePassword: async ({ input }) => {
    const { id, oldPassword, newPassword } = input;
    try {
      const { error } = passwordValidate.validate(input);
      if (error) throw new Error(error.details[0].message);
      const user = await User.findById(id);

      if (!user) {
        throw new Error("user does not exist");
      }
      const hash = await bcrypt.compare(oldPassword, user.password);
      if (!hash) {
        throw new Error("password does not match");
      }
      const newHash = await bcrypt.hash(newPassword, 10);
      user.password = newHash;
      await user.save();
      return createResult({
        data: user,
        message: "Changed password successfully",
        status: 200,
      });
    } catch (err) {
      throw err || "Change password failed";
    }
  },
};
