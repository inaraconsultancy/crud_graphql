import React, { useEffect, useState } from "react";
import { GET_ALL_USER } from "../graphql/user.jsx";
import { useQuery } from "@apollo/client";
import Navbar from "../components/Navbar.jsx";
const All_user = () => {
  const { loading, error, data } = useQuery(GET_ALL_USER);
  const [users, setUser] = useState([]);
  useEffect(() => {
    if (data) {
      setUser(data.getAllUser);
    }
  }, [data]);

  if (loading) {
    return <p>Loading...</p>;
  }
  if (error) {
    return <p>Error ..{error.message}</p>;
  }
  return (
    <>
    <Navbar/>
    <div>
      <h1>users are</h1>
      {users?.map((user, index) => {
        return (
          <div key={index}>
            <p>name is {user.name}</p>
            <p>email is {user.email}</p>
            <hr />
          </div>
        );
      })}
    </div>
    </>
  );
};

export default All_user;
