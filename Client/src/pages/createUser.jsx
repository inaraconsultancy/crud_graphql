import { useMutation } from "@apollo/client";
import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import { CREATE_USER } from "../graphql/user";

const CreateUser = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [name, setName] = useState("");
  const navigate = useNavigate();
  const [createUser, { error, data, loading }] = useMutation(CREATE_USER);

  const handleSubmit = async (e) => {
    e.preventDefault();
    const { data } = await createUser({
      variables: {name:name, email: email, password: password },
    });
    setEmail("");
    setName("");
    setPassword("");
    console.log(data);
    alert("User created successfully");
    navigate("/user");
  };
  return (
    <div>
      <label htmlFor="name">name</label>
      <input
        type="text"
        placeholder="Enter your name"
        id="name"
        value={name}
        onChange={(e) => {
          setName(e.target.value);
        }}
      />
      <br />
      <label htmlFor="email">Email</label>
      <input
        type="email"
        placeholder="Enter your email"
        id="email"
        onChange={(e) => {
          setEmail(e.target.value);
        }}
        value={email}
      />
      <br />
      <label htmlFor="password">password</label>
      <input
        type="password"
        placeholder="Enter your password"
        id="password"
        value={password}
        onChange={(e) => {
          setPassword(e.target.value);
        }}
      />
      <br />
      <button onClick={handleSubmit}>create</button>
    </div>
  );
};

export default CreateUser;
