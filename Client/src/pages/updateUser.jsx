import { useMutation, useQuery } from "@apollo/client";
import React, { useEffect, useState } from "react";
import { UPDATE_USER, GET_USER } from "../graphql/user";
import { useNavigate } from "react-router-dom";

const UpdateUser = () => {
  const [email, setEmail] = useState("");

  const [name, setName] = useState("");
  const navigate = useNavigate();
  const [updateUser, { error1, data1, loading1 }] = useMutation(UPDATE_USER);
  const { loading, error, data } = useQuery(GET_USER, {
    variables: {
      id: localStorage.getItem("id"),
      token: localStorage.getItem("token"),
    },
  });

  const handleSubmit = async (e) => {
    e.preventDefault();
    const { data } = await updateUser({
      variables: {
        id: localStorage.getItem("id"),
        name: name,
        email: email,
        token: localStorage.getItem("token"),
      },
    });
    setEmail("");
    setName("");
    alert("User updated successfully");
    navigate("/user/id");
  };
  useEffect(() => {
    if (data) {
      setEmail(data.getUser.data.email);
      setName(data.getUser.data.name);
    }
  }, [data]);
  return (
    <div>
      <h1>Update User</h1>
      <label htmlFor="name">name</label>
      <input
        type="text"
        placeholder="Enter your name"
        id="name"
        value={name}
        onChange={(e) => {
          setName(e.target.value);
        }}
      />
      <br />
      <label htmlFor="email">Email</label>
      <input
        type="email"
        placeholder="Enter your email"
        id="email"
        onChange={(e) => {
          setEmail(e.target.value);
        }}
        value={email}
      />
      <br />

      <button onClick={handleSubmit}>update</button>
    </div>
  );
};

export default UpdateUser;
