import React, { useEffect } from "react";
import { GET_USER ,DELETE_USER} from "../graphql/user.jsx";
import { useQuery,useMutation } from "@apollo/client";
import { useNavigate } from "react-router-dom";
const GetUser = () => {
  const { loading, error, data } = useQuery(GET_USER, {
    variables: {
      id: localStorage.getItem("id"),
      token: localStorage.getItem("token"),
    },
  });
    const [deleteUser, { error1, data1, loading1 }] = useMutation(DELETE_USER);
  const navigate=useNavigate();
  // useEffect(() => {
  //   getUser;
  // }, []);
  const handleLogout=()=>{
    localStorage.removeItem("id")
    localStorage.removeItem("token");
    navigate("/user")
  }
  const handleDelete=async()=>{
    const { data1 } = await deleteUser({
      variables: {
        id: localStorage.getItem("id"),
        token: localStorage.getItem("token"),
      },
    });
    console.log(data1)
    alert("User deleted successfully");
    localStorage.removeItem("id");
    localStorage.removeItem("token");
    navigate("/user");
    
  }
  return (
    <div>
      <p>{data?.getUser.data.email}</p>
      <p>{data?.getUser.data.name}</p>
      <button onClick={handleDelete}>Delete</button>
      <button onClick={()=>navigate("/update")}>Update</button>
      <button onClick={handleLogout}>Log out</button>
    </div>
  );
};

export default GetUser;
