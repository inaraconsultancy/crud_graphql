import { useMutation } from "@apollo/client";
import React, { useState } from "react";
import { useNavigate, useSearchParams } from "react-router-dom";
import { LOGIN_USER } from "../graphql/user.jsx";
import Navbar from "../components/Navbar.jsx";

const LoginUser = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const [loginUser, { error, data, loading }] = useMutation(LOGIN_USER);
  const navigate = useNavigate();
  const handleSubmit = async (e) => {
    e.preventDefault();
    const { data } = await loginUser({
      variables: { email: email, password: password },
    });
    setEmail("");

    setPassword("");

    localStorage.setItem("id", data?.loginUser.data.id);
    localStorage.setItem("token", data?.loginUser.token);
    navigate("/user/id");
  };

  
  return (
    <>
    <Navbar/>
    <div>
      <label htmlFor="email">Email</label>
      <input
        type="email"
        placeholder="Enter your email"
        id="email"
        onChange={(e) => {
          setEmail(e.target.value);
        }}
        value={email}
      />
      <br />
      <label htmlFor="password">password</label>
      <input
        type="password"
        placeholder="Enter your password"
        id="password"
        value={password}
        onChange={(e) => {
          setPassword(e.target.value);
        }}
      />
      <br />
      <button onClick={handleSubmit}>Login</button>
      <button onClick={()=>navigate("/create")}>Create New</button>

    </div>
    </>
  );
};

export default LoginUser;
