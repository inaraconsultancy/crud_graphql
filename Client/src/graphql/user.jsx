import { gql } from "@apollo/client";

export const GET_ALL_USER = gql`
  query {
    getAllUser {
      id
      name
      email
    }
  }
`;
export const GET_USER = gql`
  query getUser($id: String!, $token: String) {
    getUser(input: { id: $id, token: $token }) {
   data{
    id,
     name,
     email
   }
      message
      status
    }
  }
`;
export const LOGIN_USER = gql`
  mutation loginUser($email: String!, $password: String!) {
    loginUser(input: { email: $email, password: $password }) {
      data {
        id
      }
      message
      status
      token
    }
  }
`;
export const UPDATE_USER = gql`
  mutation updateUser($id:String!,$name: String!, $email: String!, $token: String!) {
    updateUser(input: {id:$id name:$name,email: $email, token:$token }) {
      data {
        id,
        name,
        email
      }
      message
      status
    
    }
  }
`;
export const CREATE_USER = gql`
  mutation createUser($name: String!, $email: String!, $password: String!) {
    createUser(input: { name:$name,email: $email, password: $password }) {
      data {
        id,
        name,
        email
      }
      message
      status
    
    }
  }
`;
export const DELETE_USER = gql`
  mutation deleteUser($id: String!, $token:String) {
    deleteUser(input: { id:$id,token: $token }) {
      data {
        id,
        name,
        email
      }
      message
      status
    
    }
  }
`;
