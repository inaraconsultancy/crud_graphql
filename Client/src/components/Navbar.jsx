import React from "react";
import { useNavigate } from "react-router-dom";

const Navbar = () => {
  const Navigate = useNavigate();
  return (
    <div style={{ position: "fixed", top: 0 }}>
      <button onClick={() => Navigate("/alluser")}>Show all user</button>
      <button onClick={() => Navigate("/user")}>Login</button>
    </div>
  );
};

export default Navbar;
