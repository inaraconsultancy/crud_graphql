import { useState } from "react";
import reactLogo from "./assets/react.svg";
import viteLogo from "/vite.svg";
import "./App.css";
import All_user from "./pages/all_user.jsx";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import GetUser from "./pages/getUser.jsx";
import Navbar from "./components/Navbar.jsx";
import LoginUser from "./pages/loginUser.jsx";
import CreateUser from "./pages/createUser.jsx";

import UpdateUser from "./pages/updateUser.jsx";
function App() {
  return (
    <>
      <Router>
        <Routes>
          <Route exact path="/alluser" element={<All_user />} />
          <Route exact path="/user" element={<LoginUser />} />
          <Route exact path="/user/id" element={<GetUser />} />
          <Route exact path="/create" element={<CreateUser />} />
          <Route exact path="/update" element={<UpdateUser />} />
        </Routes>
      </Router>
    </>
  );
}

export default App;
