import Joi from "joi";

export const UserValidate = Joi.object({
  name: Joi.string().required(),
  password: Joi.string().min(8).max(256).required(),
  email: Joi.string().email().required(),
});
export const LoginValidate = Joi.object({
  password: Joi.string().min(4).max(256).required(),
  email: Joi.string().email().required(),
});
export const passwordValidate=Joi.object({
  id: Joi.string().required(),
  newPassword:Joi.string().min(8).max(256).required(),
  oldPassword:Joi.string().min(4).max(256).required()
})
