import { verifyToken } from "./jwtToken.js";

export const createResult = ({ data, message, status, token }) => {
  const newData = token
    ? {
        status,
        message,
        data: data || "",
        token,
      }
    : {
        status,
        message,
        data: data || "",
      };
  return newData;
};
export const validateToken =async (token, id) => {
  if (!token) {
    throw new Error("No Token Provided");
  }
  const tokenVerify=await verifyToken(token);
  if(!tokenVerify){throw new Error("token is not correct or session is over");}
  if(id && tokenVerify.id !== id){
    throw new Error("This user doesnot have access to this resource")
  }

};
