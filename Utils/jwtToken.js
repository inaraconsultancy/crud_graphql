import jwt from "jsonwebtoken";
const secretKey = process.env.SECRET_KEY;
import dotenv from "dotenv";
dotenv.config();
export const createToken = async ({ id, email }) => {
  const token = await jwt.sign({ id, email }, process.env.SECRET_KEY, {
    expiresIn: 3600 * 24 * 7,
    algorithm: "HS256",
  });
  return token;
};
export const verifyToken = async (token) => {
  const tokenVerified = await jwt.verify(
    token,
    process.env.SECRET_KEY,
    (err, decoded) => {
      if (err) {
        if (err.message === "jwt expired") {
          throw new Error("session expired, login again");
        }
      } else {
        return decoded;
      }
    }
  );
  return tokenVerified;
};
